﻿using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Services;
using Fare;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private const string PATTERN = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

        private static readonly Regex Validator = new(PATTERN, RegexOptions.None);
        private static readonly Xeger Generator = new(PATTERN);

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(!Vehicle.IsValid(id))
            {
                throw new ArgumentException("Uncorrect id format");
            }
            Id = id;
            
            if(!Enum.IsDefined<VehicleType>(vehicleType))
            {
                throw new ArgumentException("Uncorrrect type of vehicle");
            }
            VehicleType = vehicleType;

            if (balance <= 0)
            {
                throw new ArgumentException("Initial balance must be positive");
            }
            Balance = balance;
        }

		public Vehicle(VehicleType vehicleType, decimal balance)
            :this(GenerateRandomRegistrationPlateNumber(), vehicleType, balance)
		{

		}

		public Vehicle()
		{

		}

		[JsonProperty]
		public string Id { get; set; }

		[JsonProperty]
		public VehicleType VehicleType { get; set; }

		[JsonProperty]
		public decimal Balance { get; set; }

        public static string GenerateRandomRegistrationPlateNumber() => Generator.Generate();
        public static bool IsValid(string number) => Validator.IsMatch(number);

        public override string ToString()
        {
            return $"{Id}: {VehicleType}. Balance: {Balance}";
        }
    }
}
