﻿namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar = 1,
        Truck,
        Bus,
        Motorcycle
    }
}
