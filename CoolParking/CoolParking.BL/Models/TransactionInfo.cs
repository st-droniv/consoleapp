﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime Time { get;}

        public string VehicleId { get;}

        public decimal Sum { get;}

        public TransactionInfo(DateTime time, string vehicleId, decimal sum)
        {
            Time = time;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{VehicleId}: {Sum} at {Time}";
        }
    }
}