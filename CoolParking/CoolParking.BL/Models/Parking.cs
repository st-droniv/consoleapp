﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
	public class Parking: IDisposable
	{
		private static Parking _parking;
		private static readonly object _lock = new();

		private Parking()
		{
			Vehicles = new(Settings.MaxCapacity);
			Balance = Settings.InitialBalance;
		}

		public static Parking GetInstance()
		{
			if (_parking == null)
			{
				lock (_lock)
				{
					if (_parking == null)
					{
						_parking = new();
					}
				}
			}

			return _parking;
		}

		public void Dispose()
		{
			Vehicles.Clear();
			Balance = Settings.InitialBalance;
		}

		public List<Vehicle> Vehicles { get; }
		public decimal Balance { get; set; }
	}
}