﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
	public static class Settings
	{
		public static decimal InitialBalance { get; } = 0;
		public static int MaxCapacity { get; } = 10;
		public static double WithdrawTime { get; } = 5_000;
		public static double LogTime { get; } = 60_000;
		public static Dictionary<VehicleType, decimal> WithdrawRate { get; } = new()
		{
			{ VehicleType.PassengerCar, 2},
			{ VehicleType.Truck, 5},
			{ VehicleType.Bus, 3.5M},
			{ VehicleType.Motorcycle, 1},
		};
		public static decimal FineRate { get; } = 2.5M;

		public static string LogPath { get; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions-{DateTime.Now.Day}.{DateTime.Now.Month}.{DateTime.Now.Year}.log";
	}
}