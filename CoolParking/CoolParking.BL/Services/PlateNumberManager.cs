﻿using Fare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
	public static class PlateNumberManager
	{
		private const string PATTERN = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

		private static readonly Regex Validator = new(PATTERN, RegexOptions.None);
		private static readonly Xeger Generator = new(PATTERN);

		public static bool IsValid(string number) => Validator.IsMatch(number);
		public static string Generate() => Generator.Generate();
	}
}
