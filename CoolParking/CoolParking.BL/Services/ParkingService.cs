﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
	public class ParkingService: IParkingService
	{
		private readonly ITimerService _withdrawTimer;
		private readonly ITimerService _logTimer;
		private readonly ILogService _logService;

		private readonly Parking _parking = Parking.GetInstance();

		public List<TransactionInfo> Transactions { get; } = new();

		public ParkingService(ITimerService withdrawTimer, 
			ITimerService logTimer, 
			ILogService logService)
		{
			_withdrawTimer = withdrawTimer;
			_logTimer = logTimer;
			_logService = logService;

			SetUpTimers();
		}

		private void SetUpTimers()
		{
			_withdrawTimer.Interval = Settings.WithdrawTime;
			_logTimer.Interval = Settings.LogTime;

			_withdrawTimer.Elapsed += WithdrawTimerEventHandler;
			_logTimer.Elapsed += LogTimerEventHandler;

			_withdrawTimer.Start();
			_logTimer.Start();
		}

		public void WithdrawTimerEventHandler(object sender, ElapsedEventArgs e)
		{
			foreach (var vehicle in _parking.Vehicles)
			{
				Transactions.Add(Withdraw(vehicle, e?.SignalTime ?? DateTime.UtcNow));
			}
		}

		public void LogTimerEventHandler(object sender, ElapsedEventArgs e)
		{
			if (Transactions.Any())
			{
				lock (this)
				{
					foreach (var transactionInfo in Transactions)
					{
						_logService.Write($"{transactionInfo.Time}: {transactionInfo.Sum} money withdraw from vehicle with Id='{transactionInfo.VehicleId}'");
					}
					Transactions.Clear();
				}

				return;
			}
			_logService.Write("No transactions happend.");
		}

		private TransactionInfo Withdraw(Vehicle vehicle, DateTime time)
		{
			decimal paymentSum = 0;

			if (vehicle.Balance > 0 && vehicle.Balance >= Settings.WithdrawRate[vehicle.VehicleType])
			{
				paymentSum = Settings.WithdrawRate[vehicle.VehicleType];
			}
			else if (vehicle.Balance > 0 && vehicle.Balance < Settings.WithdrawRate[vehicle.VehicleType])
			{
				var negative = Settings.WithdrawRate[vehicle.VehicleType] - vehicle.Balance;
				paymentSum = vehicle.Balance + negative * Settings.FineRate;
			}
			else
			{
				paymentSum = Settings.WithdrawRate[vehicle.VehicleType] * Settings.FineRate;
			}

			vehicle.Balance -= paymentSum;
			_parking.Balance += paymentSum;

			return new TransactionInfo(time, vehicle.Id, paymentSum);
		}

		public decimal GetBalance()
		{
			return _parking.Balance;
		}

		public int GetCapacity()
		{
			return _parking.Vehicles.Capacity;
		}

		public int GetFreePlaces()
		{
			return  Settings.MaxCapacity - _parking.Vehicles.Count;
		}

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			return _parking.Vehicles.AsReadOnly();
		}

		public void AddVehicle(Vehicle vehicle)
		{
			if (_parking.Vehicles.Count >= Settings.MaxCapacity)
				throw new InvalidOperationException("No parking places available.");

			if (_parking.Vehicles.Exists(v => v.Id.Equals(vehicle.Id)))
				throw new ArgumentException("Trying to add vehicle with exicting plate number.");

			_parking.Vehicles.Add(vehicle);
		}

		public void RemoveVehicle(string vehicleId)
		{
			if (!_parking.Vehicles.Exists(v => v.Id.Equals(vehicleId)))
				throw new ArgumentException("Trying to remove unexisting vehicle.");

			var vehicle = _parking.Vehicles.Single(v => v.Id.Equals(vehicleId));

			if (vehicle.Balance < 0)
				throw new InvalidOperationException($"You must to pay debt {vehicle.Balance} and only then you will be able to leave.");

			_parking.Vehicles.Remove(vehicle);
		}

		public void TopUpVehicle(string vehicleId, decimal sum)
		{
			if (sum < 0)
				throw new ArgumentException("Can not top up a negative value.");

			if (!_parking.Vehicles.Exists(v => v.Id.Equals(vehicleId)))
				throw new ArgumentException("Trying to top up unexisting vehicle."); ;

			var vehicle = _parking.Vehicles.Single(v => v.Id.Equals(vehicleId));

			vehicle.Balance += sum;
		}

		public TransactionInfo[] GetLastParkingTransactions()
		{
			return Transactions.ToArray();
		}

		public string ReadFromLog()
		{
			return _logService.Read();
		}

		public void Dispose()
		{
			_withdrawTimer.Dispose();
			_logTimer.Dispose();
			_parking.Dispose();
		}
	}
}