﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
	public class LogService : ILogService
	{
		public LogService(string path)
		{
			LogPath = path;
		}

		public LogService()
			:this(Settings.LogPath)
		{

		}

		public string LogPath { get; }

		public string Read()
		{
			if (!File.Exists(LogPath))
				throw new InvalidOperationException($"File: {LogPath} doesn't exists.");

			return File.ReadAllText(LogPath);
		}

		public void Write(string logInfo)
		{
			if (!File.Exists(LogPath))
			{
				File.WriteAllText(LogPath, logInfo + "\n");
				return;
			}

			File.AppendAllText(LogPath, logInfo + "\n");
		}
	}
}