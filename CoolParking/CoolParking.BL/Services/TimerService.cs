﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
	public class TimerService : ITimerService
	{
		private readonly Timer _timer;

		public TimerService(double interval)
		{
			_timer = new Timer(interval);
		}

		public TimerService()
		{
			_timer = new();
		}

		public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

		public event ElapsedEventHandler Elapsed
		{
			add
			{
				_timer.Elapsed += value;
			}

			remove
			{
				_timer.Elapsed -= value;
			}
		}

		public void Dispose()
		{
			_timer.Dispose();
		}

		public void Start()
		{
			_timer.AutoReset = true;
			_timer.Start();
		}

		public void Stop()
		{
			_timer.Stop();
		}
	}
}
