﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp
{
	public class NetworkParkingService : IParkingService
	{
		private readonly HttpClient _httpClient;

		public NetworkParkingService(Uri baseUrl)
		{
			_httpClient = new HttpClient();
			_httpClient.BaseAddress = baseUrl;
		}

		private async Task<T> SendRequestWithoutContent<T>(string url, HttpMethod method)
		{
			var request = new HttpRequestMessage(method, url);

			var response = await _httpClient.SendAsync(request);
			var respContent = await response.Content.ReadAsStringAsync();

			response.EnsureSuccessStatusCode();

			var res = JsonConvert.DeserializeObject<T>(respContent);
			return res;
		}

		private async Task SendRequestWithoutResponse<T>(string url, HttpMethod method, T content)
		{
			var request = new HttpRequestMessage(method, url);

			if (content != null)
			{
				var cont = JsonConvert.SerializeObject(content);
				request.Content = new StringContent(cont, Encoding.UTF8, "application/json");
			}

			var response = await _httpClient.SendAsync(request);
			response.EnsureSuccessStatusCode();
		}

		public void AddVehicle(Vehicle vehicle)
		{
			try
			{
				SendRequestWithoutResponse<Vehicle>("vehicles", HttpMethod.Post, vehicle).Wait();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to add new vehicle");
			}
		}

		public void Dispose()
		{
			_httpClient.Dispose();
		}

		public decimal GetBalance()
		{
			decimal res = -1;
			try
			{
				res = (SendRequestWithoutContent<decimal>("parking/balance", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get balance");
			}

			return res;
		}

		public int GetCapacity()
		{
			int res = -1;
			try
			{
				res = (SendRequestWithoutContent<int>("parking/capacity", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get balance");
			}

			return res;
		}

		public int GetFreePlaces()
		{
			int res = -1;
			try
			{
				res = (SendRequestWithoutContent<int>("parking/freePlaces", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get free places");
			}

			return res;
		}

		public TransactionInfo[] GetLastParkingTransactions()
		{
			TransactionInfo[] res = default;
			try
			{
				res = (SendRequestWithoutContent<TransactionInfo[]>("transactions/last", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get last transactions");
			}

			return res;
		}

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			IEnumerable<Vehicle> res = default;
			try
			{
				res = (SendRequestWithoutContent<IEnumerable<Vehicle>>("vehicles", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get vehicles");
			}

			return new ReadOnlyCollection<Vehicle>(res.ToList());
		}

		public string ReadFromLog()
		{
			string res = string.Empty;
			try
			{
				res = (SendRequestWithoutContent<string>("transactions/all", HttpMethod.Get)).Result;
			}
			catch (Exception _)
			{
				Console.WriteLine("Failed to get all transactions");
			}

			return res;
		}

		public void RemoveVehicle(string vehicleId)
		{
			try
			{
				SendRequestWithoutResponse<string>("vehicles", HttpMethod.Post, vehicleId).Wait();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to delete vehicle");
			}
		}

		public void TopUpVehicle(string vehicleId, decimal sum)
		{
			try
			{
				SendRequestWithoutResponse<object>("transactions/topUpVehicle", HttpMethod.Put, new { id = vehicleId, balance = sum }).Wait();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to top up vehicle {0}", vehicleId);
			}
		}
	}
}
