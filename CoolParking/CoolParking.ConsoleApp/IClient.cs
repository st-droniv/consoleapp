﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp
{
	public interface IClient
	{
		void Run();
	}
}
