﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleApp
{
	public class ConsoleClient: IClient
	{
		private readonly IParkingService _parkingService;

		public ConsoleClient(IParkingService parkingService)
		{
			_parkingService = parkingService;
		}

		public void Run()
		{
			Console.WriteLine("___Welcome to Parking Management App___");
			int opt = 0;

			while (true)
			{
				PrintMenu();
				int.TryParse(Console.ReadLine(), out opt);

				switch (opt)
				{
					case 1:
						{
							Console.WriteLine($"Current balance - {_parkingService.GetBalance()}");
							break;
						}

					case 2:
						{
							Console.WriteLine($"Amount of earned money (before logging into file) - {_parkingService.GetLastParkingTransactions().Sum(t => t.Sum)}");
							break;
						}

					case 3:
						{
							Console.WriteLine($"Amount of current free places - {_parkingService.GetFreePlaces()}");
							break;
						}

					case 4:
						{
							Console.WriteLine("Corrent transactions (before logging):");
							foreach (var t in _parkingService.GetLastParkingTransactions())
							{
								Console.WriteLine(t);
							}
							break;
						}

					case 5:
						{
							try
							{
								Console.WriteLine("Alltime transactions:");
								Console.WriteLine(_parkingService.ReadFromLog());
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message);
							}
							break;
						}

					case 6:
						{
							Console.WriteLine("Current vehicles:");
							var vehicles = _parkingService.GetVehicles();
							if (vehicles != null)
							{
								foreach (var v in vehicles)
								{
									Console.WriteLine(v);
								}
							}
							break;
						}

					case 7:
						{
							Console.WriteLine("Enter the intial vehicle balance: ");
							Decimal.TryParse(Console.ReadLine(), out decimal balance);

							Console.WriteLine("Choose type of vehicle: ");
							PrintVehicleTypeOptions();
							Enum.TryParse<VehicleType>(Console.ReadLine(), out VehicleType vehicleType);

							Console.WriteLine("DO you want to enter plate number of your vehicle?[Y] - yes");

							Vehicle v = default;
							if (Console.ReadKey().KeyChar.Equals('Y'))
							{
								Console.WriteLine("\nEnter plate number:");
								var number = new string(Console.ReadLine()).Trim();

								v = new Vehicle(number, vehicleType, balance);
							}
							else
							{
								v = new Vehicle(vehicleType, balance);
							}

							try
							{
								_parkingService.AddVehicle(v);
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message);
							}

							break;
						}

					case 8:
						{
							Console.WriteLine("Enter vehicle plate number: ");
							var vehicleId = Console.ReadLine();

							try
							{
								_parkingService.RemoveVehicle(vehicleId);
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message);
							}

							break;
						}

					case 9:
						{
							Console.WriteLine("Enter vehicle plate number: ");
							var vehicleId = Console.ReadLine();

							Console.WriteLine("Enter the amount: ");
							Decimal.TryParse(Console.ReadLine(), out decimal sum);

							try
							{
								_parkingService.TopUpVehicle(vehicleId, sum);
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message);
							}

							break;
						}

					default:
						{
							_parkingService.Dispose();
							return;
						}
				}
			}
		}

		static void PrintMenu()
		{
			Console.WriteLine("\n\nChoose an action:");
			Console.WriteLine("1 - Print current Parking balance.");
			Console.WriteLine("2 - Print the amount of earned money (before logging into file).");
			Console.WriteLine("3 - Print current free places.");
			Console.WriteLine("4 - Print all current transactions (before logging into file).");
			Console.WriteLine("5 - Print all transactions.");
			Console.WriteLine("6 - Print current Parking vehicles.");
			Console.WriteLine("7 - Place vehicle into the parking lot.");
			Console.WriteLine("8 - Pick up the car from the parking lot.");
			Console.WriteLine("9 - Top up vehicle balance.");
			Console.WriteLine("0 - Exit\n");
		}

		static void PrintVehicleTypeOptions()
		{
			var names = Enum.GetNames(typeof(VehicleType));

			for (int i = 0; i < names.Length; i++)
			{
				Console.WriteLine($"{i+1} - {names[i]}");
			}
		}
	}
}
