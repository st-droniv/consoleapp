﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace CoolParking.ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			var localParking = SetUpLocalService();
			//var networkParking = SetUpNetworkService();
			IClient client = new ConsoleClient(localParking);

			client.Run();
		}

		static IParkingService SetUpLocalService()
		{
			return new ParkingService(new TimerService(Settings.WithdrawTime), new TimerService(Settings.LogTime), new LogService());
		}

		static IParkingService SetUpNetworkService()
		{
			return new NetworkParkingService(new Uri("https://localhost:5001/api/"));
		}
	}
}
