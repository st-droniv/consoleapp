﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
	public class VehicleUpdateDTO
	{
		public string Id { get; set; }

		public decimal Balance { get; set; }
	}
}
