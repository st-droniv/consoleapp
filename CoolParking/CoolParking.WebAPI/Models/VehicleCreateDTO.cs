﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
	public class VehicleCreateDTO
	{
		public string Id { get; set; }

		public VehicleType VehicleType { get; set; }

		public decimal Balance { get; set; }
	}
}
