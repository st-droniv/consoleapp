﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Configuration
{
	public class VehicleCreateDTOValidator: AbstractValidator<VehicleCreateDTO>
	{
		public VehicleCreateDTOValidator()
		{
			CascadeMode = CascadeMode.StopOnFirstFailure;

			RuleFor(v => v.Id).NotNull().Must(id => Vehicle.IsValid(id));
			RuleFor(v => v.VehicleType).IsInEnum();
			RuleFor(v => v.Balance).GreaterThan(0);
		}
	}
}
