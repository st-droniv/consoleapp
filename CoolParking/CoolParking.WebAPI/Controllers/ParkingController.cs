﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
	[ApiController]
	public class ParkingController : BaseController
	{
		[HttpGet]
		[Route("balance")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(decimal))]
		public IActionResult GetBalance()
		{
			return Ok(ParkingService.GetBalance());
		}

		[HttpGet]
		[Route("capacity")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
		public IActionResult GetCapacity()
		{
			return Ok(ParkingService.GetCapacity());
		}

		[HttpGet]
		[Route("freePlaces")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
		public IActionResult GetFreePlaces()
		{
			return Ok(ParkingService.GetFreePlaces());
		}
	}
}
