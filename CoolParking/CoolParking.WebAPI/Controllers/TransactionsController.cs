﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
	[ApiController]
	public class TransactionsController : BaseController
	{
		private readonly IValidator<VehicleUpdateDTO> _validator;

		public TransactionsController(IValidator<VehicleUpdateDTO> validator)
		{
			_validator = validator;
		}

		[HttpGet]
		[Route("last")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TransactionInfo[]))]
		public IActionResult GetLastTransactions()
		{
			return Ok(ParkingService.GetLastParkingTransactions());
		}

		[HttpGet]
		[Route("all")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
		public IActionResult GetAllTransactions()
		{
			return Ok(ParkingService.ReadFromLog());
		}

		[HttpPut]
		[Route("topUpVehicle")]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Vehicle))]
		public IActionResult ModifyBalance(VehicleUpdateDTO dto)
		{
			var res = _validator.Validate(dto);
			if (!res.IsValid)
				return BadRequest(res.Errors);

			var vehicle = ParkingService.GetVehicles().SingleOrDefault(v => v.Id.Equals(dto.Id));
			if (vehicle == null)
				return NotFound(dto.Id);

			vehicle.Balance += dto.Balance;
			return Ok(vehicle);
		}
	}
}
