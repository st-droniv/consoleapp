﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
	[ApiController]
	public class VehiclesController : BaseController
	{
		private readonly IValidator<VehicleCreateDTO> _validator;

		public VehiclesController(IValidator<VehicleCreateDTO> validator)
		{
			_validator = validator;
		}


		[HttpGet]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IReadOnlyCollection<Vehicle>))]
		public IActionResult Get()
		{
			return Ok(ParkingService.GetVehicles());
		}

		[HttpGet]
		[Route("{id}")]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Vehicle))]
		public IActionResult Get(string id)
		{
			if (!Vehicle.IsValid(id))
				return BadRequest("Invalid id");

			var vehicles = ParkingService.GetVehicles();
			var vehicle = vehicles.SingleOrDefault(v => v.Id.Equals(id));

			if (vehicle == null)
				return NotFound(id);

			return Ok(vehicle);
		}

		[HttpPost]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Vehicle))]
		public IActionResult Create(VehicleCreateDTO dto)
		{
			var res = _validator.Validate(dto);
			if (!res.IsValid)
				return BadRequest(res.Errors);

			var vehicle = new Vehicle(dto.Id, dto.VehicleType, dto.Balance);
			try
			{
				ParkingService.AddVehicle(vehicle);
			}
			catch (Exception ex)
			{
				//log somowhere
				return BadRequest(ex.Message);
			}

			return Created(Url.Action("Create", "Vehicles", dto), vehicle);
		}

		[HttpDelete]
		[Route("{id}")]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		public IActionResult Delete(string id)
		{
			if(!Vehicle.IsValid(id))
				return BadRequest("Invalid id");

			try
			{
				ParkingService.RemoveVehicle(id);
			}
			catch (ArgumentException ex)
			{
				return NotFound(id);
			}
			catch (InvalidOperationException ex)
			{
				return BadRequest(ex.Message);
			}

			return NoContent();
		}
	}
}
